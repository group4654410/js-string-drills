let assert = require("assert");
const string4 = require("../string4.cjs");

const result1 = "John Smith";
const result2 = "John Doe Smith";
assert.deepEqual(result1, string4({"first_name": "JoHN", "last_name": "SMith"}));
assert.deepEqual(result2, string4({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}));