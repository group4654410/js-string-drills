let assert = require("assert");
const string1 = require("../string1.cjs");

const result1 = 100.45;
const result2 = 1002.22;
const result3 = -123;
assert.deepEqual(result1, string1("$100.45"));
assert.deepEqual(result2, string1("$1,002.22"));
assert.deepEqual(result3, string1("-$123"));