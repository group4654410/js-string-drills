let assert = require("assert");
const string5 = require("../string5.cjs");

const result = "the quick brown fox.";
assert.deepEqual(result, string5(["the", "quick", "brown", "fox"]));