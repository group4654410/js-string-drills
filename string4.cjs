function getFullName(nameObj) {

    let result = [];

    if (nameObj !== undefined && typeof nameObj === 'object') {
        let first_name = nameObj.first_name ? result.push(nameObj.first_name) : ""
        let middle_name = nameObj.middle_name ? result.push(nameObj.middle_name) : ""
        let last_name = nameObj.last_name ? result.push(nameObj.last_name) : ""

        result = result.map((word) => {
            return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
        });
    }

    return result.join(" ");
}

module.exports = getFullName;