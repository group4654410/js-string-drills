function monthOfDate(dateString) {

    let result = "";

    if (dateString !== undefined && typeof dateString === 'string') {
        result = Intl.DateTimeFormat('en', {month: "long"}).format(new Date(dateString));
    }
    
    return result;
}

module.exports = monthOfDate;