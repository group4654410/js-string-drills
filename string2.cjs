function ipv4Components(ipAddress) {
    if (ipAddress !== undefined && typeof ipAddress === 'string') {

        ipAddress = ipAddress.trim();
        let components = ipAddress.split(".");

        const isNumberAndBelow255 = (val) => { return !isNaN(val) && val >= 0 && val <= 255 };

        if (components.length === 4 && components.every(isNumberAndBelow255)) {
            components = components.map((num) => {
                return parseInt(num);
            });

            return components;
        }
    }
    return [];
}

module.exports = ipv4Components;