function joinArrayOfStrings(wordArray) {

    const allElementsAreString = (val) => { return typeof val === 'string' };

    if (wordArray !== undefined && Array.isArray(wordArray)) {
        if (wordArray.every(allElementsAreString) && wordArray.length > 0) {
            return wordArray.join(" ") + ".";
        }
    }

    return "";
}

module.exports = joinArrayOfStrings;