function toNumbers(number) {
    let result = 0;
    if (number !== undefined && typeof number === 'string') {

        number = number.trim();
        number = number.replaceAll(",", "");

        if (number.startsWith("$")) {
            number = number.slice(1);
        } else if (number.startsWith("-$") || number.startsWith("+$")) {
            number = number.substring(0, 1) + number.substring(2);
        }

        if (!isNaN(number)) {
            result = Number(number);
        }

    }
    return result;
}

module.exports = toNumbers;